import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  border-radius: 8px;
  background-color: #fff;
  width: 30.9%;
  padding: 8px;
`;

export const Infos = styled.div`
  margin-left: 16px;
`;
