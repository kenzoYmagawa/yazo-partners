import { Avatar, Chip, Typography } from "@mui/material";
import { PartnerItemProps } from "./partnerItem.interfaces";
import { Container, Infos } from "./partnerItem.styles";
import { lighten } from "polished";

const PartnerItem = ({
  name,
  type,
  chipColor,
  description,
  avatar,
}: PartnerItemProps) => {
  return (
    <Container>
      <Avatar src={avatar} />

      <Infos>
        <Typography fontWeight="bold" fontSize={18}>
          {name}
        </Typography>
        <Chip
          label={type}
          size="small"
          style={{
            background: lighten(0.4, chipColor || "#999"),
            color: chipColor,
          }}
        />
        <Typography color="#777" style={{ marginTop: 16 }}>
          {description}
        </Typography>
      </Infos>
    </Container>
  );
};

export { PartnerItem };
