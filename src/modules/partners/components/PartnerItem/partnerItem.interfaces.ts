export interface PartnerItemProps {
  name: string;
  avatar?: string;
  type: "productor" | "agency" | "freelancer";
  description?: string;
  chipColor?: string;
}