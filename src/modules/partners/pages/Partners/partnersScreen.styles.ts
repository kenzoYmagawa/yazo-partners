import styled from 'styled-components';

export const Container = styled.div`
  padding-bottom: 16px;
`;

export const PartnerList = styled.div`
  margin-top: 24px;
  display: flex;
  flex-wrap: wrap;
  gap: 16px;
`;