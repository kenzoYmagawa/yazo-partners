import { IconButton, Tooltip, Typography } from "@mui/material";
import { Searchbar } from "../../../../shared/components/Searchbar";
import { PartnerItem } from "../../components/PartnerItem/PartnerItem";
import { PartnerItemProps } from "../../components/PartnerItem/partnerItem.interfaces";
import { Container, PartnerList } from "./partnersScreen.styles";
import { FilterList } from "@mui/icons-material";

interface IPartner extends PartnerItemProps {
  id: number;
}

const partners: IPartner[] = [
  {
    id: 1,
    name: "Yazo Agency",
    type: "agency",
    chipColor: "#1E90FF",
    description:
      "Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using",
  },
  {
    id: 2,
    name: "João Bastos",
    type: "freelancer",
    chipColor: "#9932CC",
    description:
      "Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using",
  },
  {
    id: 3,
    name: "Frezarim Eventos",
    type: "productor",
    chipColor: "#B22222",
    description:
      "Distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using",
  },
];

const Partners = () => {
  return (
    <Container>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography fontWeight="bold" fontSize={28} color="#444">
          Parceiros Homologados
        </Typography>

        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Tooltip title="Filtrar parceiros" style={{ marginRight: 8 }}>
            <IconButton>
              <FilterList fontSize="small" />
            </IconButton>
          </Tooltip>
          <Searchbar />
        </div>
      </div>
      <PartnerList>
        {[...partners, ...partners, ...partners].map((partner) => (
          <PartnerItem key={partner.id} {...partner} />
        ))}
      </PartnerList>
    </Container>
  );
};

export { Partners };
