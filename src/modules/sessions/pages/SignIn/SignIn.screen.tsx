import { Typography } from "@mui/material";
import { Input } from "../../../../shared/components/atoms/Input";
import { Container, Background, Form } from "./signIn.styles";
import { Email, Lock } from "@mui/icons-material";
import { useState } from "react";
import { useAuthContext } from "../../../../layers/auth/contexts/auth.context";
import { Button } from "../../../../shared/components/atoms/Button/Button";

const SignIn = () => {
  const { signIn, isSignIn } = useAuthContext();

  //* STATES
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  //* FUNCTIONS
  const onSubmit = () => {
    signIn({ uid: email, password });
  };

  return (
    <Container>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          onSubmit();
        }}
      >
        <Typography fontWeight="bold" fontSize={30} color="#444">
          Faça seu login
        </Typography>

        <Typography fontSize={14} color="#aaa" style={{ marginBottom: 24 }}>
          Todas as informações para seu evento em um só lugar
        </Typography>

        <Input
          value={email}
          onChange={setEmail}
          placeholder="Digite seu email"
          style={{ marginBottom: 16 }}
          icon={
            <Email
              style={{ marginRight: 8, fill: "#00000065" }}
              fontSize="small"
            />
          }
        />

        <Input
          value={password}
          onChange={setPassword}
          placeholder="Digite sua senha"
          type="password"
          style={{ marginBottom: 24 }}
          icon={
            <Lock
              style={{ marginRight: 8, fill: "#00000065" }}
              fontSize="small"
            />
          }
        />

        <Button type="submit" isLoading={isSignIn} size="large">
          Logar
        </Button>
      </Form>
      <Background src="https://s3.sa-east-1.amazonaws.com/development-localhost/files/1643553022093.png" />
    </Container>
  );
};

export { SignIn };
