import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  padding: 8px;
  align-items: flex-start;
`;

export const Form = styled.form`
  flex: 1;
  align-self: center;
  align-items: center;
  padding: 16px;
`;

export const Background = styled.img`
  height: calc(100vh - 16px);
  width: 75%;
  object-fit: cover;
  border-radius: 16px;
`;
