export interface IUser {
  id: string;
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  remember_me_token?: any;
  avatar_url?: any;
  company?: any;
  occupation?: any;
  company_url?: any;
  phone?: any;
  cookie_accepted_at?: any;
  is_online?: boolean;
  is_blocked?: boolean;
}
