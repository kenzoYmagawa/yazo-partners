import { Typography } from "@mui/material";
import React from "react";
import { EventItem } from "../../../../shared/components/molecules/EventItem/EventItem";

//* CUSTOM IMPORTS
import { Container, EventContainer } from "./eventsScreen.styles";

interface IEvent {
  id: string;
  imageSource: string;
  title: string;
  description: string;
  peopleCount?: number;
  calendarContent?: string;
}

const events: IEvent[] = [
  {
    id: "1",
    imageSource:
      "https://s3.sa-east-1.amazonaws.com/development-localhost/files/1643496685982.png",
    title: "Yazo Evento",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    peopleCount: 1000,
    calendarContent: "01 até 14 de Janeiro de 2022",
  },
  {
    id: "2",
    imageSource:
      "https://s3.sa-east-1.amazonaws.com/development-localhost/files/1643498091036.png",
    title: "Yazo Evento 2",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    peopleCount: 2050,
    calendarContent: "01 até 14 de Janeiro de 2022",
  },
  {
    id: "3",
    imageSource:
      "https://s3.sa-east-1.amazonaws.com/development-localhost/files/1643496685982.png",
    title: "Yazo Evento",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    peopleCount: 3769,
    calendarContent: "01 até 14 de Janeiro de 2022",
  },
  {
    id: "4",
    imageSource:
      "https://s3.sa-east-1.amazonaws.com/development-localhost/files/1643498091036.png",
    title: "Yazo Evento 2",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    calendarContent: "01 até 14 de Janeiro de 2022",
    peopleCount: 5769,
  },
];

const Events: React.FC = () => {
  return (
    <Container>
      <Typography fontWeight="bold" fontSize={28} color="#444">
        Meus Eventos
      </Typography>

      <EventContainer>
        {[...events, ...events].map((event) => (
          <EventItem
            key={event.id}
            title={event.title}
            imageSource={event.imageSource}
            description={event.description}
            peopleCount={event.peopleCount}
            calendarContent={event.calendarContent}
          />
        ))}
      </EventContainer>
    </Container>
  );
};

export { Events };
