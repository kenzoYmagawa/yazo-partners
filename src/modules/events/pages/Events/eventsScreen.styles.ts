import styled from "styled-components";

export const Container = styled.div``;

export const EventContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 16px;
  margin-top: 24px;
`;
