export interface NegotiationItemProps {
  title: string;
  status: "pending" | "success" | "failed";
  price?: string;
  deadline?: string;
}
