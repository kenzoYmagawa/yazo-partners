import styled from "styled-components";

export const Container = styled.div`
  width: calc(50% - 26px);
  background-color: #fff;
  padding: 8px;
  border-radius: 8px;
  cursor: pointer;
  transition: opacity ease 0.5s;

  &:hover {
    opacity: 0.5;
  }
`;
