import { Typography, Chip, Tooltip, IconButton } from "@mui/material";
import { NegotiationItemProps } from "./negotiationItem.interfaces";
import { Container } from "./negotiationItem.styles";
import { MobileFriendlyOutlined, Computer } from "@mui/icons-material";
import { colors } from "../../../../shared/styles/colors";

export const NegotiationItem = ({
  title,
  price,
  status,
  deadline,
}: NegotiationItemProps) => {
  const getColorByStatus = () => {
    switch (status) {
      case "success":
        return "success";

      case "failed":
        return "error";

      default:
        break;
    }
  };

  return (
    <Container>
      <div
        style={{
          display: "flex",
          alignContent: "center",
          justifyContent: "space-between",
        }}
      >
        <div>
          <Typography fontWeight="bold" fontSize={18} color="#444">
            {title}
          </Typography>

          <Typography fontSize={14} color="#aaa">
            {deadline}
          </Typography>
        </div>

        <Typography fontWeight="bold" fontSize={16} color="#444">
          {price}
        </Typography>
      </div>

      <div
        style={{
          display: "flex",
          alignContent: "center",
          width: "100%",
          marginTop: 8,
        }}
      >
        <div
          style={{
            display: "flex",
            alignContent: "center",
            width: "100%",
          }}
        >
          <Tooltip title="Plataforma">
            <IconButton>
              <Computer fontSize="small" style={{ color: colors.primary }} />
            </IconButton>
          </Tooltip>

          <Tooltip title="Aplicativo">
            <IconButton>
              <MobileFriendlyOutlined
                fontSize="small"
                style={{ color: colors.primary }}
              />
            </IconButton>
          </Tooltip>
        </div>

        <Chip
          label={status}
          color={getColorByStatus()}
          style={{ marginTop: 8 }}
        />
      </div>
    </Container>
  );
};
