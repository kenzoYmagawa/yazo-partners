import styled from "styled-components";

export const Container = styled.div`
  height: "100%";
  border-left: 1px solid #00000050;
  flex: 1;
  margin-left: 16px;
  padding: 0 16px; 
`;
