import { Typography } from "@mui/material";
import { Container } from "./negotiationCheck.styles";

interface NegotiationCheckProps {
  selectedOption: "platform" | "app" | "platform+app";
  usersCount: string;
}

const NegotiationCheck = ({
  selectedOption,
  usersCount,
}: NegotiationCheckProps) => {
  const getProduct = () => {
    if (selectedOption === "platform")
      return <Typography fontWeight="bold">Plataforma</Typography>;

    if (selectedOption === "app")
      return <Typography fontWeight="bold">Aplicativo</Typography>;

    if (selectedOption === "platform+app")
      return <Typography fontWeight="bold">Aplicativo + Plataforma</Typography>;
  };

  return (
    <Container>
      <div>
        <Typography
          fontWeight="bold"
          fontSize={20}
          color="#444"
          style={{ marginBottom: 24 }}
        >
          Resumo
        </Typography>

        {getProduct()}

        <Typography>Setup</Typography>
        <Typography color="#aaa" style={{ marginBottom: 16 }}>
          R$ 5000.00
        </Typography>

        {usersCount && (
          <>
            <Typography>{usersCount} usuários simultaneos</Typography>
            <Typography color="#aaa">
              R${Number(usersCount) * 2.5}.00
            </Typography>
          </>
        )}
      </div>
    </Container>
  );
};

export { NegotiationCheck };
