import { ToggleButton, ToggleButtonGroup, Typography } from "@mui/material";
import {
  MobileFriendly,
  ComputerOutlined,
  CalendarTodayOutlined,
  PeopleOutline,
} from "@mui/icons-material";
import {
  Container,
  Content,
  ItemTypography,
  Form,
  Input,
} from "./requestNegotiationScreen.styles";
import { colors } from "../../../../shared/styles/colors";
import { useState } from "react";
import { NegotiationCheck } from "./NegotiationCheck/NegotiationCheck";

export const RequestNegotiation = () => {
  //* STATES
  const [selectedOption, setSelectedOption] = useState<
    "platform" | "app" | "platform+app"
  >("platform");
  const [eventName, setEventName] = useState("");
  const [usersCount, setUsersCount] = useState("");

  return (
    <Container>
      <Typography
        fontWeight="bold"
        fontSize={28}
        color="#444"
        style={{ marginBottom: 24 }}
      >
        Nova negociação
      </Typography>

      <ToggleButtonGroup
        // value={alignment}
        exclusive
        // onChange={handleAlignment}
        aria-label="text alignment"
      >
        <ToggleButton
          value="left"
          onClick={() => setSelectedOption("platform")}
          aria-label="left aligned"
          style={
            selectedOption === "platform"
              ? {
                  backgroundColor: colors.primary,
                  color: "#fff",
                }
              : undefined
          }
        >
          <ComputerOutlined />
          <ItemTypography>Plataforma</ItemTypography>
        </ToggleButton>
        <ToggleButton
          value="center"
          onClick={() => setSelectedOption("app")}
          aria-label="centered"
          style={
            selectedOption === "app"
              ? {
                  backgroundColor: colors.primary,
                  color: "#fff",
                }
              : undefined
          }
        >
          <MobileFriendly />
          <ItemTypography>Aplicativo</ItemTypography>
        </ToggleButton>
        <ToggleButton
          onClick={() => setSelectedOption("platform+app")}
          value="right"
          aria-label="right aligned"
          style={
            selectedOption === "platform+app"
              ? {
                  backgroundColor: colors.primary,
                  color: "#fff",
                }
              : undefined
          }
        >
          <ComputerOutlined /> + <MobileFriendly />
        </ToggleButton>
      </ToggleButtonGroup>

      <Content>
        <Form>
          <div style={{ display: "flex", alignItems: "center", gap: 8 }}>
            <Input placeholder="Digite o nome do evento" />
            <Input
              value={usersCount}
              onChange={setUsersCount}
              placeholder="Quantidade de participantes"
              icon={
                <PeopleOutline fontSize="small" style={{ marginRight: 8 }} />
              }
            />
          </div>

          <div style={{ display: "flex", alignItems: "center", gap: 8 }}>
            <Input
              placeholder="Data de Início do evento"
              type="date"
              icon={
                <CalendarTodayOutlined
                  fontSize="small"
                  style={{ marginRight: 8 }}
                />
              }
            />
            <Input
              type="date"
              placeholder="Data de Término do evento"
              icon={
                <CalendarTodayOutlined
                  fontSize="small"
                  style={{ marginRight: 8 }}
                />
              }
            />
          </div>
        </Form>

        <NegotiationCheck
          selectedOption={selectedOption}
          usersCount={usersCount}
        />
      </Content>
    </Container>
  );
};
