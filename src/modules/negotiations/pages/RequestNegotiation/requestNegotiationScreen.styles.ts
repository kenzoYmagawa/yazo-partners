import { Input as InputC } from "../../../../shared/components/atoms/Input";
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
`;

export const Content = styled.div`
  display: flex;
  flex: 1;
  margin-top: 32px;
  justify-content: space-between;
`;

export const ItemTypography = styled.h5`
  margin: 0;
  margin-left: 16px;
`;

export const Input = styled(InputC)`
  margin-bottom: 16px;
  flex: 1;
`;

export const Form = styled.form`
  flex: 1;
  max-width: 50%;
`;
