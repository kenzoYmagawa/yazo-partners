import { Fab, Typography } from "@mui/material";
import { NegotiationItem } from "../../components/NegotiationItem/NegotiationItem";
import AddIcon from "@mui/icons-material/Add";
import { NegotiationItemProps } from "../../components/NegotiationItem/negotiationItem.interfaces";

import {
  Container,
  NegotiationsContainer,
  Header,
} from "./negotiations.styles";
import { useNavigate } from "react-router-dom";

interface Negotiation extends NegotiationItemProps {
  id: string;
}

const negotiations: Negotiation[] = [
  {
    id: "1",
    status: "pending",
    title: "Negociação com VTEX",
    deadline: "Fechamento no dia 22 de Janeiro",
    price: "R$156.000,00",
  },
  {
    id: "2",
    status: "success",
    title: "Negociação com VTEX",
    deadline: "Fechamento no dia 22 de Janeiro",
    price: "R$156.000,00",
  },
  {
    id: "3",
    status: "failed",
    title: "Negociação com VTEX",
    deadline: "Fechamento no dia 22 de Janeiro",
    price: "R$156.000,00",
  },
  {
    id: "4",
    status: "success",
    title: "Negociação com VTEX",
    deadline: "Fechamento no dia 22 de Janeiro",
    price: "R$156.000,00",
  },
  {
    id: "5",
    status: "success",
    title: "Negociação com VTEX",
    deadline: "Fechamento no dia 22 de Janeiro",
    price: "R$156.000,00",
  },
];

const Negotiations = () => {
  const navigate = useNavigate();

  return (
    <Container>
      <Header>
        <Typography fontWeight="bold" fontSize={28} color="#444">
          Negociações
        </Typography>

        <Fab
          color="primary"
          aria-label="add"
          size="small"
          onClick={() => navigate("request")}
        >
          <AddIcon />
        </Fab>
      </Header>

      <NegotiationsContainer>
        {negotiations.map((negotiation) => (
          <NegotiationItem key={negotiation.id} {...negotiation} />
        ))}
      </NegotiationsContainer>
    </Container>
  );
};

export { Negotiations };
