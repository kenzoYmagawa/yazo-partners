import { IUser } from "../../../modules/users/interfaces/users.interfaces";

export namespace IAuthAPIs {
  export interface SessionRequest {
    uid: string;
    password: string;
  }

  export interface SessionResponse {
    user: IUser;
    token: {
      token: string;
    };
  }
}
