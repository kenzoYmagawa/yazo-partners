import { api } from "../../../shared/services/api";
import { AxiosResponse } from "axios";
import { IAuthAPIs } from "../interfaces/auth.apis.interfaces";

export const createSession = (
  data: IAuthAPIs.SessionRequest
): Promise<AxiosResponse<IAuthAPIs.SessionResponse>> =>
  api.post("/login", data);
