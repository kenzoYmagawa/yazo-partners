import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { useNavigate } from "react-router-dom";
import { IUser } from "../../../modules/users/interfaces/users.interfaces";
import { setBearerToken } from "../../../shared/services/api";
import { createSession } from "../apis/auth.apis";
import { IAuthAPIs } from "../interfaces/auth.apis.interfaces";

interface AuthContextData {
  signIn: (data: IAuthAPIs.SessionRequest) => Promise<void>;
  signOut: () => void;
  isSignIn: boolean;
  loggedUser: IUser;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [loggedUser, setLoggedUser] = useState<IUser>({} as IUser);
  const [isSignIn, setIsSignIn] = useState(false);

  //* FUNCTIONS
  useEffect(() => {
    const persistedUser = localStorage.getItem("loggedUser");

    if (persistedUser) setLoggedUser(JSON.parse(persistedUser));
  }, []);

  const signIn = useCallback(async (data: IAuthAPIs.SessionRequest) => {
    try {
      setIsSignIn(true);

      const {
        data: {
          user,
          token: { token },
        },
      } = await createSession(data);

      localStorage.setItem("loggedUser", JSON.stringify(user));

      setLoggedUser(user);
      setBearerToken(token);
      setIsSignIn(false);
    } catch (error) {
      setIsSignIn(false);
    }
  }, []);

  const signOut = useCallback(() => {
    setLoggedUser({} as IUser);
    localStorage.clear();
  }, []);

  return (
    <AuthContext.Provider value={{ loggedUser, isSignIn, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

const useAuthContext = () => {
  const context = useContext(AuthContext);

  if (!context) throw new Error("useAuthContext must be within AuthProvider");

  return context;
};

export { AuthProvider, useAuthContext };
