import { createContext, useContext, useState } from "react";
import {
  DateRangeOutlined,
  FactCheckOutlined,
  PeopleOutlined,
} from "@mui/icons-material";

//* CUSTOM IMPORTS
import { Header } from "../../../shared/components/Header";
import { SideMenu } from "../../../shared/components/SideMenu/SideMenu";
import { useAuthContext } from "../../auth/contexts/auth.context";
import { useNavigate } from "react-router-dom";

const SignedContext = createContext({});

const SignedProvider: React.FC = ({ children }) => {
  const { signOut, loggedUser } = useAuthContext();
  const navigate = useNavigate();
  const [menuId, setMenuId] = useState<number | undefined>();

  const onSideMenuClick = (id: number) => {
    setMenuId(id);

    switch (id) {
      case 2:
        navigate("/negotiations");
        break;

      case 3:
        navigate("/partners");
        break;

      default:
        navigate("/events");
        break;
    }
  };

  return (
    <SignedContext.Provider value={{}}>
      <Header
        onSignOut={signOut}
        userName={loggedUser.username}
        userAvatar={loggedUser.avatar_url}
      />
      <SideMenu
        isSelected={menuId}
        onItemClick={(item) => onSideMenuClick(item.id)}
        items={[
          { id: 1, content: "Meus Eventos", icon: <DateRangeOutlined /> },
          { id: 2, content: "Negociações", icon: <FactCheckOutlined /> },
          { id: 3, content: "Parceiros", icon: <PeopleOutlined /> },
        ]}
      />

      <div
        className="signedChilden"
        style={{
          position: "relative",
          left: 264,
          top: "calc(56px + 24px)",
          borderRadius: 8,
          width: "calc(100vw - 330px)",
        }}
      >
        {children}
      </div>
    </SignedContext.Provider>
  );
};

const useSignedContext = () => {
  const context = useContext(SignedContext);

  if (!context)
    throw new Error("useSignedContext must be within SignedProvider");

  return context;
};

export { SignedProvider, useSignedContext };
