import React from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { AppRoutes } from "./routes/routes";
import { colors } from "./styles/colors";

import "./styles/global.css";
import { AuthProvider } from "../layers/auth/contexts/auth.context";

const theme = createTheme({
  palette: {
    primary: {
      main: colors.primary,
    },
    secondary: {
      main: colors.primary,
    },
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <AuthProvider>
          <AppRoutes />
        </AuthProvider>
      </ThemeProvider>
    </div>
  );
}

export default App;
