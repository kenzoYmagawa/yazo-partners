import axios from 'axios';

export const api = axios.create({
  baseURL: 'https://yazo-partners-api.herokuapp.com'
})

export const setBearerToken = (token: string) => {
  axios.defaults.headers.common['Auth-Token'] = `Bearer ${token}`;
};
