export interface HeaderProps {
  onSignOut?: () => void;
  onShowProfile?: () => void;
  userName: string;
  userAvatar?: string;
}
