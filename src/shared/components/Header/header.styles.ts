import styled from "styled-components";

export { SIDE_MENU_WIDTH } from "../SideMenu/sideMenu.styles";

export const Container = styled.div`
  flex-direction: row;
  display: flex;
  align-items: center;
  position: absolute;
  height: 64px;
  width: calc(100vw - 240px - 64px);
  z-index: 2;
  justify-content: space-between;
  top: 0;
  right: 0;
  padding: 0 32px;
  /* border-bottom: 1px solid #ccc; */
  background-color: #fff;
`;

export const AvatarContainer = styled.div`
  flex-direction: row;
  display: flex;
  align-items: center;
`;
