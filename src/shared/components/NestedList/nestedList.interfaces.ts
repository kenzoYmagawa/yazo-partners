import { ReactElement } from "react";

export interface NestedItemProps {
  id: number;
  content: string;
  icon?: ReactElement;
}

export interface NestedListProps {
  style?: React.CSSProperties;
  items: NestedItemProps[];
  onItemClick?: (item: NestedItemProps) => void;
  isSelected?: number;
}
