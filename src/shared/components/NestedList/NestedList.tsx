import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { colors } from "../../styles/colors";
import { NestedListProps } from "./nestedList.interfaces";

export function NestedList({
  style,
  items,
  isSelected,
  onItemClick = () => {},
}: NestedListProps) {
  return (
    <List
      style={style}
      sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
      component="nav"
      aria-labelledby="nested-list-subheader"
    >
      {items.map((item) => (
        <ListItemButton
          key={item.id}
          onClick={() => onItemClick(item)}
          style={
            isSelected === item.id
              ? {
                  backgroundColor: colors.primary,
                  color: "#fff",
                }
              : undefined
          }
        >
          <ListItemIcon
            style={isSelected === item.id ? { color: "#fff" } : undefined}
          >
            {item.icon}
          </ListItemIcon>
          <ListItemText primary={item.content} />
        </ListItemButton>
      ))}
    </List>
  );
}
