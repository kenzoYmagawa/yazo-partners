import { Input } from "../atoms/Input";
import Search from "@mui/icons-material/Search";

const Searchbar = () => {
  return (
    <Input
      style={{ width: 250 }}
      icon={<Search style={{ fill: "#00000050", marginRight: 4 }} />}
      placeholder="Pesquisa rápida"
    />
  );
};

export { Searchbar };
