export interface EventItemProps {
  title: string;
  imageSource: string;
  description?: string;
  peopleCount?: number;
  calendarContent?: string;
}
