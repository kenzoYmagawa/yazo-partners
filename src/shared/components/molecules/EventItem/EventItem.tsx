import { Typography } from "@mui/material";
import { EventItemProps } from "./eventItem.interfaces";
import { Container, Image, InfoContainer, Icons } from "./eventItem.styles";
import { People, CalendarToday } from "@mui/icons-material";
import { colors } from "../../../styles/colors";

export const EventItem = ({
  imageSource,
  title,
  description,
  peopleCount,
  calendarContent,
}: EventItemProps) => {
  return (
    <Container>
      <Image src={imageSource} />

      <InfoContainer>
        <div>
          <Typography fontWeight="bold" fontSize={20} color="#333">
            {title}
          </Typography>
          <Typography fontSize={14} color="#aaa">
            {description}
          </Typography>
        </div>

        <Icons>
          {calendarContent && (
            <>
              <CalendarToday
                fontSize="small"
                style={{
                  fill: colors.primary,
                  position: "relative",
                  bottom: 1,
                }}
              />
              <Typography
                fontWeight="bold"
                fontSize={12}
                style={{ marginLeft: 8, color: colors.primary }}
              >
                {calendarContent}
              </Typography>
            </>
          )}

          {peopleCount && (
            <div
              style={{ marginLeft: 16, display: "flex", alignItems: "center" }}
            >
              <People fontSize="small" style={{ fill: colors.primary }} />
              <Typography
                fontWeight="bold"
                fontSize={12}
                style={{ marginLeft: 8, color: colors.primary }}
              >
                {peopleCount}
              </Typography>
            </div>
          )}
        </Icons>
      </InfoContainer>
    </Container>
  );
};
