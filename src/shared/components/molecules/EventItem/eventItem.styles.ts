import styled from "styled-components";

export const Container = styled.div`
  width: 31%;
  background-color: #fff;
  margin-bottom: 16px;
  border-radius: 8px;
  cursor: pointer;
  transition: opacity ease 0.5s;
  /* box-shadow: 0px 4px 16px #00000015; */

  &:hover {
    opacity: 0.75;
  }
`;

export const Image = styled.img`
  width: 100%;
  border-radius: 8px 8px 0 0;
`;

export const InfoContainer = styled.div`
  padding: 16px;
`;

export const Icons = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin-top: 40px;
`;
