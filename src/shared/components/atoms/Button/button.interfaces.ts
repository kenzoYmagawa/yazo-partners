export interface ButtonProps {
  children: string;
  isLoading?: boolean;
  size?: "small" | "large" | "medium" | undefined;
  onClick?: () => void;
  type?: "button" | "submit" | "reset" | undefined;
}
