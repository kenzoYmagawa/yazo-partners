import { Button as MuiButton, CircularProgress } from "@mui/material";
import { ButtonProps } from "./button.interfaces";

export const Button = ({ children, isLoading, size, type, ...rest }: ButtonProps) => {
  return (
    <MuiButton
      type={type}
      variant="contained"
      size={size}
      style={{
        width: "100%",
        borderRadius: 8,
      }}
      {...rest}
    >
      {isLoading ? (
        <CircularProgress size={20} style={{ color: "#fff" }} />
      ) : (
        children
      )}
    </MuiButton>
  );
};
