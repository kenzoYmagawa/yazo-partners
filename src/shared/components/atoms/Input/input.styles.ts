import styled from "styled-components";

export const Container = styled.div`
  border-radius: 8px;
  height: 50px;
  display: flex;
  align-items: center;
  padding: 0 16px;
  background-color: #00000010;
`;

export const InputElement = styled.input`
  outline: none;
  border: none;
  border-radius: 8px;
  height: 30px;
  width: 100%;
  background: none;
`;
