//* CUSTOM IMPORTS
import { Container, InputElement } from "./input.styles";
import { InputProps } from "./input.interfaces";

export const Input = ({
  style,
  icon,
  placeholder,
  defaultValue,
  onChange = () => {},
  value,
  type,
  className,
}: InputProps) => {
  return (
    <Container style={style} className={className}>
      {icon}
      <InputElement
        type={type}
        placeholder={placeholder}
        onChange={(e) => onChange(e.target.value)}
        value={value}
        defaultValue={defaultValue}
      />
    </Container>
  );
};
