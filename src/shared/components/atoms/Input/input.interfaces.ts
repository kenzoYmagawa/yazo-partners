import { CSSProperties, ReactElement } from "react";

export interface InputProps {
  style?: CSSProperties;
  icon?: ReactElement;
  placeholder?: string;
  onChange?: (value: string) => void;
  value?: string;
  defaultValue?: string;
  type?: React.HTMLInputTypeAttribute | undefined;
  className?: string;
}
