import { CircularProgress } from "@mui/material";

export const Spinner = () => {
  return <CircularProgress size={16} />;
};
