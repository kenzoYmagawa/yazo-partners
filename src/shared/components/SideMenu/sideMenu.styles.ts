import styled from 'styled-components';

export const SIDE_MENU_WIDTH = 240;
export const SIDE_MENU_PADDING = 24;

export const Container = styled.div`
  background: #fff;
  width: ${SIDE_MENU_WIDTH}px;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  padding: 16px 0;
  box-shadow: 3px 6px 16px #00000010;
  z-index: 3;
`;

export const Logo = styled.img`
  height: 20px;
  position: relative;
  left: 16px;
`;

