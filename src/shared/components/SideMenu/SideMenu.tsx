import logo from "../../assets/logo.png";

//* CUSTOM IMPORTS
import { SideMenuProps } from "./sideMenu.interfaces";
import { Container, Logo } from "./sideMenu.styles";
import { NestedList } from "../NestedList";

const SideMenu = ({ items, onItemClick, isSelected }: SideMenuProps) => {
  return (
    <Container>
      <Logo src={logo} />

      <NestedList
        isSelected={isSelected}
        style={{ marginTop: 32 }}
        items={items}
        onItemClick={onItemClick}
      />
    </Container>
  );
};

export { SideMenu };
