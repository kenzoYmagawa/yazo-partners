import { NestedItemProps } from "../NestedList/nestedList.interfaces";

export interface SideMenuProps {
  items: NestedItemProps[];
  onItemClick?: (item: NestedItemProps) => void;
  isSelected?: number;
}
