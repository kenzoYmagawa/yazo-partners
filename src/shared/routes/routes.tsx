import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useAuthContext } from "../../layers/auth/contexts/auth.context";
import { SignedProvider } from "../../layers/signed/contexts/signed.context";
import { Negotiations } from "../../modules/negotiations/pages/Negotiations/Negotiations.screen";
import { Events } from "../../modules/events/pages/Events/Events.screen";
import { SignIn } from "../../modules/sessions/pages/SignIn/SignIn.screen";
import { RequestNegotiation } from "../../modules/negotiations/pages/RequestNegotiation/RequestNegociation.screen";
import { Partners } from "../../modules/partners/pages/Partners/Partners.screen";

const AppRoutes: React.FC = () => {
  const { loggedUser } = useAuthContext();

  return (
    <BrowserRouter>
      {loggedUser.id ? (
        <SignedProvider>
          <Routes>
            <Route path="/events" element={<Events />} />
            <Route path="/negotiations" element={<Negotiations />} />
            <Route
              path="/negotiations/request"
              element={<RequestNegotiation />}
            />
             <Route
              path="/partners"
              element={<Partners />}
            />
          </Routes>
        </SignedProvider>
      ) : (
        <Routes>
          <Route path="/login" element={<SignIn />} />
          <Route path="/" element={<SignIn />} />
        </Routes>
      )}
    </BrowserRouter>
  );
};

export { AppRoutes };
