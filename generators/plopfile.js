module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'design',
        message: 'What is your atomic design level (atoms, molecules, etc...)?',
      },
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name in pascalCase?',
      },
      {
        type: 'input',
        name: 'camel',
        message: 'What is your component name in camelCase?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: '../src/shared/components/{{design}}/{{pascalCase name}}/{{pascalCase name}}.tsx',
        templateFile: 'templates/Component.tsx.hbs',
      },
      {
        type: 'add',
        path: '../src/shared/components/{{design}}/{{pascalCase name}}/{{camelCase camel}}.styles.ts',
        templateFile: 'templates/styles.ts.hbs',
      },
      {
        type: 'add',
        path: '../src/shared/components/{{design}}/{{pascalCase name}}/{{pascalCase name}}.stories.tsx',
        templateFile: 'templates/stories.tsx.hbs',
      },
      {
        type: 'add',
        path: '../src/shared/components/{{design}}/{{pascalCase name}}/{{camelCase camel}}.interfaces.ts',
        templateFile: 'templates/interface.ts.hbs',
      },
      {
        type: 'add',
        path: '../src/shared/components/{{design}}/{{pascalCase name}}/{{pascalCase name}}.spec.tsx',
        templateFile: 'templates/test.tsx.hbs',
      },
      {
        type: 'add',
        path: '../src/shared/components/{{design}}/{{pascalCase name}}/index.ts',
        templateFile: 'templates/index.ts.hbs',
      },
    ],
  });
};
